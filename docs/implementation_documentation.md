# Implementační dokumentace modulu _municipal-police-stations_

## Záměr

Modul slouží k ukládání a poskytování informací o Municipal Police Stations.

## Vstupní data

**Dostupní poskytovatelé**:

-   opendata IPR ArcGIS Hub

### Data aktivně stahujeme

#### _open data IPR_

-   zdroj dat
    -   url: [config.datasources.MunicipalPoliceStations](https://services5.arcgis.com/SBTXIEUGWbqzUecw/arcgis/rest/services/FSB_CUR_BEZ_OBJEKTMPP_B/FeatureServer/0/query?outFields=*&where=1%3D1&f=geojson&resultRecordCount=1000&resultOffset=0)
-   formát dat
    -   protokol: http
    -   datový typ: json
    -   validační schéma: [municipalPoliceStationsJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/municipal-police-stations/-/blob/development/src/integration-engine/datasources/MunicipalPoliceStationsDataSource.ts)
    -   příklad [vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/municipal-police-stations/-/blob/development/test/integration-engine/data/municipalpolicestations-datasource.json)
-   frekvence stahování
    -   cron definice:
        -   cron.dataplatform.municipalpolicestations.refreshDataInDB
            -   rabin `0 31 7 * * *`
            -   prod `0 25 2 1 * *`
-   název rabbitmq fronty
    -   dataplatform.municipalpolicestations.refreshDataInDB

## Zpracování dat / transformace

Při transformaci data obohacujeme o atribut `district`, která vychází z polohy daného sběrného dvoru a získává se metodou `cityDistrictsModel.getDistrict` v modulu city-district.

### MunicipalPoliceStations

#### _task: RefreshDataInDBTask_

-   vstupní rabbitmq fronta
    -   název: dataplatform.municipalpolicestations.refreshDataInDB
    -   bez parametrů
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: dataplatform.municipalpolicestations.updateAddressAndDistrict
    -   parametry: `{ id: policeStations.id }`
-   datové zdroje
    -   dataSource IPR
-   transformace
    -   [MunicipalPoliceStationsTransformation.ts](https://gitlab.com/operator-ict/golemio/code/modules/municipal-police-stations/-/blob/development/src/integration-engine/transformations/MunicipalPoliceStationsTransformation.ts) - mapování pro `PoliceStationsModel`
-   obohacení dat
    -   viď `UpdateAddressAndDistrictTask`
-   data modely
    -   policeStationsModel -> (schéma municipal_police_stations) `policeStations`

#### _task: UpdateAddressAndDistrictTask_

-   vstupní rabbitmq fronta
    -   název: dataplatform.municipalpolicestations.updateAddressAndDistrict
    -   parametry: `{ id: policeStations.id }`
-   datové zdroje
    -   `city-districts` module
-   data modely
    -   policeStationsModel -> (schéma municipal_police_stations) `policeStations.district`

## Uložení dat

-   typ databáze
    -   PSQL
-   databázové schéma
    -   ![municipalpolicestations er diagram](./assets/municipalpolicestations_erd.png)
-   retence dat
    -   `policeStationsModel` update

## Output API

`MunicipalPoliceStationsRouter` implementuje `GeoJsonRouter`.

### Obecné

-   OpenAPI v3 dokumentace
    -   [OpenAPI](./openapi.yaml)
-   api je veřejné
-   postman kolekce
    -   TBD
-   Asyncapi dokumentace RabbitMQ front
    -   [AsyncAPI](./asyncapi.yaml)

#### _/municipalpolicestations_

-   zdrojové tabulky
    -   `police_stations`
-   dodatečná transformace: Feature
-

#### _/municipalpolicestations_/:id\_

-   zdrojové tabulky
    -   `police_stations`
-   dodatečná transformace: Feature collection
