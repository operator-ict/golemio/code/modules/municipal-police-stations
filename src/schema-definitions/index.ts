import { municipalPoliceStationsDatasource } from "#sch/datasources/MunicipalPoliceStationsJsonSchema";
import { municipalPoliceStations } from "#sch/MunicipalPoliceStations";

const forExport: any = {
    name: "MunicipalPoliceStations",
    pgSchema: "municipal_police_stations",
    datasources: {
        municipalPoliceStationsDatasource,
    },
    definitions: {
        municipalPoliceStations,
    },
};

export { forExport as MunicipalPoliceStations };
