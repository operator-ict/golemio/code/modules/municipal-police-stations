import { Point } from "geojson";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";

export interface IMunicipalPoliceStationsFeature {
    type: string;
    id: number;
    geometry: Point;
    properties: {
        CPOP: number | null;
        NKU: string | null;
        NVPK: string | null;
        OBJECTID: number;
        POZN: string;
        GLOBALID: string;
        OR_MP: string;
    };
}

const municipalPoliceStationsJsonSchema: JSONSchemaType<IMunicipalPoliceStationsFeature[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            geometry: { $ref: "#/definitions/geometry" },
            properties: {
                type: "object",
                properties: {
                    CPOP: { oneOf: [{ type: "integer" }, { type: "null" }] },
                    NKU: { oneOf: [{ type: "string" }, { type: "null" }] },
                    NVPK: { oneOf: [{ type: "string" }, { type: "null" }] },
                    OBJECTID: { type: "integer" },
                    POZN: { type: "string" },
                    GLOBALID: { type: "string" },
                    OR_MP: { type: "string" },
                },
                required: ["NKU", "OBJECTID", "POZN"],
                additionalProperties: true,
            },
            type: { type: "string" },
            id: { type: "number" },
        },
        required: ["geometry", "properties", "type"],
        additionalProperties: false,
    },
    definitions: {
        // @ts-expect-error
        geometry: SharedSchemaProvider.Geometry,
    },
};

export const municipalPoliceStationsDatasource: { name: string; jsonSchema: JSONSchemaType<IMunicipalPoliceStationsFeature[]> } =
    {
        name: "MunicipalPoliceStationsDatasource",
        jsonSchema: municipalPoliceStationsJsonSchema,
    };
