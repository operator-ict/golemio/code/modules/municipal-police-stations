import { Point } from "geojson";

export interface IMunicipalPoliceStation {
    id: string;
    geometry: Point;
    cadastral_area: string;
    note: string;
    district?: string;
    address_country?: string;
    address_formatted?: string;
    address_locality?: string;
    address_region?: string;
    postal_code?: string;
    street_address?: string;
}

export const municipalPoliceStations = {
    name: "MunicipalPoliceStations",
    pgTableName: "police_stations",
};
