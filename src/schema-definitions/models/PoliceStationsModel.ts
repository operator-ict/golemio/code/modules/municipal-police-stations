import { Point } from "geojson";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";
import { IMunicipalPoliceStation } from "#sch/MunicipalPoliceStations";

export class PoliceStationsModel extends Model<IMunicipalPoliceStation> implements IMunicipalPoliceStation {
    declare id: string;
    declare geometry: Point;
    declare cadastral_area: string;
    declare note: string;
    declare district?: string;
    declare address_country?: string;
    declare address_formatted?: string;
    declare address_locality?: string;
    declare address_region?: string;
    declare postal_code?: string;
    declare street_address?: string;

    public static attributeModel: ModelAttributes<PoliceStationsModel> = {
        id: {
            type: DataTypes.STRING,
            primaryKey: true,
        },
        geometry: DataTypes.GEOMETRY,
        cadastral_area: DataTypes.STRING(255),
        note: DataTypes.STRING(255),
        district: { type: DataTypes.STRING, allowNull: true },
        address_country: { type: DataTypes.STRING },
        address_formatted: { type: DataTypes.STRING },
        address_locality: { type: DataTypes.STRING },
        address_region: { type: DataTypes.STRING },
        postal_code: { type: DataTypes.STRING },
        street_address: { type: DataTypes.STRING },
    };

    public static jsonSchema: JSONSchemaType<IMunicipalPoliceStation[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "string" },
                geometry: { $ref: "#/definitions/geometry" },
                cadastral_area: { type: "string" },
                note: { type: "string" },
                district: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
            },
            required: ["id"],
        },
        definitions: {
            // @ts-expect-error
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
