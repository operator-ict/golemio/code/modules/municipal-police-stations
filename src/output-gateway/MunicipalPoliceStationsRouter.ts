import { Router } from "@golemio/core/dist/shared/express";
import { GeoJsonRouter } from "@golemio/core/dist/output-gateway/routes";
import { MunicipalPoliceStationsRepository } from ".";

export class MunicipalPoliceStationsRouter extends GeoJsonRouter {
    constructor() {
        super(new MunicipalPoliceStationsRepository());
        this.initRoutes({ maxAge: 12 * 60 * 60, staleWhileRevalidate: 60 * 60 });
    }
}

const municipalPoliceStationsRouter: Router = new MunicipalPoliceStationsRouter().router;

export { municipalPoliceStationsRouter };
