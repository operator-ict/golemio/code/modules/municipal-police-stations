import Sequelize from "@golemio/core/dist/shared/sequelize";
import { IPropertyResponseModel, SequelizeModel } from "@golemio/core/dist/output-gateway";
import { IGeoJsonModel } from "@golemio/core/dist/output-gateway/models/interfaces/IGeoJsonModel";
import {
    buildGeojsonFeatureCollection,
    buildGeojsonFeatureLatLng,
    IGeoJsonAllFilterParameters,
    IGeoJSONFeature,
    IGeoJSONFeatureCollection,
} from "@golemio/core/dist/output-gateway";
import { MunicipalPoliceStations } from "#sch";
import { PoliceStationsModel } from "#sch/models/PoliceStationsModel";
import { FilterHelper } from "./FilterHelper";

export class MunicipalPoliceStationsRepository extends SequelizeModel implements IGeoJsonModel {
    constructor() {
        super(
            "MunicipalPoliceStationsRepository",
            MunicipalPoliceStations.definitions.municipalPoliceStations.pgTableName,
            PoliceStationsModel.attributeModel,
            { schema: MunicipalPoliceStations.pgSchema }
        );
    }

    public IsPrimaryIdNumber(idKey: string): Promise<boolean> {
        return Promise.resolve(false);
    }

    public PrimaryIdentifierSelection(id: string): object {
        return { id };
    }

    public GetProperties = async (): Promise<IPropertyResponseModel[]> => {
        // Not implemented
        return [];
    };

    public GetAll = async (options: IGeoJsonAllFilterParameters = {}): Promise<IGeoJSONFeatureCollection> => {
        const result = await this.sequelizeModel.findAll<PoliceStationsModel>({
            attributes: {
                include: ["updated_at"],
            },
            where: options
                ? {
                      [Sequelize.Op.and]: [
                          ...FilterHelper.prepareFilterForLocation(options),
                          ...FilterHelper.prepareFilterForUpdateSince(options),
                          ...FilterHelper.prepareFilterForDistricts(options),
                      ],
                  }
                : {},
            limit: options?.limit,
            offset: Number.isInteger(options?.offset) ? options?.offset : undefined,
            order: FilterHelper.prepareOrderFunction(options),
        });

        return buildGeojsonFeatureCollection(
            result.map((record: PoliceStationsModel) => {
                return this.formatOutput(record);
            })
        );
    };

    public GetOne = async (id: string): Promise<IGeoJSONFeature | undefined> => {
        const result = await this.sequelizeModel.findOne<PoliceStationsModel>({
            attributes: {
                include: ["updated_at"],
            },
            where: { id },
        });

        return result ? this.formatOutput(result) : undefined;
    };

    private formatOutput = (record: PoliceStationsModel): IGeoJSONFeature | undefined => {
        const {
            geometry,
            address_formatted,
            street_address,
            postal_code,
            address_locality,
            address_region,
            address_country,
            ...rest
        } = record.get({ plain: true });
        return buildGeojsonFeatureLatLng(
            {
                ...rest,
                ...{
                    address: {
                        address_formatted,
                        street_address,
                        postal_code,
                        address_locality,
                        address_region,
                        address_country,
                    },
                },
            },
            geometry.coordinates[0],
            geometry.coordinates[1]
        );
    };
}
