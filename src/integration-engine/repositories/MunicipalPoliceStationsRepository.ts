import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

import { MunicipalPoliceStations } from "#sch";
import { PoliceStationsModel } from "#sch/models/PoliceStationsModel";
import { IMunicipalPoliceStation } from "#sch/MunicipalPoliceStations";

export class MunicipalPoliceStationsRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            "MunicipalPoliceStationsRepository",
            {
                outputSequelizeAttributes: PoliceStationsModel.attributeModel,
                pgTableName: MunicipalPoliceStations.definitions.municipalPoliceStations.pgTableName,
                pgSchema: MunicipalPoliceStations.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator("MunicipalPoliceStationsRepositoryValidator", PoliceStationsModel.jsonSchema)
        );
    }

    public saveBulk = async (data: IMunicipalPoliceStation[]) => {
        if (await this.validate(data)) {
            const fieldsToUpdate = Object.getOwnPropertyNames(data[0])
                .filter((el) => el !== "id")
                .map<keyof IMunicipalPoliceStation>((el) => el as keyof IMunicipalPoliceStation);
            fieldsToUpdate.push("updated_at" as any);
            await this.sequelizeModel.bulkCreate<PoliceStationsModel>(data, {
                updateOnDuplicate: fieldsToUpdate,
            });
        }
    };
}
