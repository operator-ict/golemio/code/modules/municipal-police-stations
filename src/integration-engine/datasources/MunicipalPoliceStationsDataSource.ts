import {
    DataSource,
    JSONDataTypeStrategy,
    PaginatedHTTPProtocolStrategy,
} from "@golemio/core/dist/integration-engine/datasources";
import { config } from "@golemio/core/dist/integration-engine/config";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { MunicipalPoliceStations } from "#sch";

export class MunicipalPoliceStationsDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            MunicipalPoliceStations.datasources.municipalPoliceStationsDatasource.name,
            new PaginatedHTTPProtocolStrategy({
                headers: {},
                method: "GET",
                url: config.datasources.MunicipalPoliceStations,
            }),
            new JSONDataTypeStrategy({ resultsPath: "features" }),
            new JSONSchemaValidator(
                MunicipalPoliceStations.datasources.municipalPoliceStationsDatasource.name + "Validator",
                MunicipalPoliceStations.datasources.municipalPoliceStationsDatasource.jsonSchema,
                true
            )
        );
    }
}
