import slug from "slugify";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { MunicipalPoliceStations } from "#sch";
import { IMunicipalPoliceStation } from "#sch/MunicipalPoliceStations";
import { IMunicipalPoliceStationsFeature } from "#sch/datasources/MunicipalPoliceStationsJsonSchema";

export class MunicipalPoliceStationsTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = MunicipalPoliceStations.name;
    }

    public transform = async (data: IMunicipalPoliceStationsFeature[]): Promise<IMunicipalPoliceStation[]> => {
        const stations: IMunicipalPoliceStation[] = [];

        for (const item of data) {
            const transformedData = this.transformElement(item);
            stations.push(transformedData);
        }

        return stations;
    };

    protected transformElement = (element: IMunicipalPoliceStationsFeature): IMunicipalPoliceStation => {
        return {
            id: element.properties.OBJECTID.toString(),
            geometry: element.geometry,
            cadastral_area: element.properties.NKU ?? "",
            note: element.properties.POZN,
        };
    };
}
