import { PoliceStationsModel } from "#sch/models/PoliceStationsModel";
import { IMunicipalPoliceStation } from "#sch/MunicipalPoliceStations";
import CityDistrictsModel from "@golemio/city-districts/dist/integration-engine/repositories/CityDistrictPostgresRepository";
import { GeocodeApi } from "@golemio/core/dist/integration-engine";
import { RecoverableError } from "@golemio/core/dist/shared/golemio-errors";
import { Point } from "geojson";

export class AddressHelper {
    public static isGeometryChanged = (point1: Point, point2: Point): boolean => {
        return point1.coordinates[0] !== point2.coordinates[0] || point1.coordinates[1] !== point2.coordinates[1];
    };

    public static isMissing = (policeStation: PoliceStationsModel): boolean => {
        return !policeStation.address_formatted || !policeStation.address_country || !policeStation.district;
    };

    public static needsUpdate = (
        newPoliceStation: IMunicipalPoliceStation,
        currentDbPoliceStation: PoliceStationsModel
    ): boolean => {
        return (
            AddressHelper.isGeometryChanged(newPoliceStation.geometry, currentDbPoliceStation.geometry) ||
            AddressHelper.isMissing(currentDbPoliceStation)
        );
    };

    public static async updateAddress(dbData: PoliceStationsModel) {
        try {
            const address = await GeocodeApi.getAddressByLatLng(dbData.geometry.coordinates[1], dbData.geometry.coordinates[0]);
            if (address) {
                dbData.address_country = address.address_country ?? "";
                dbData.address_formatted = address.address_formatted ?? "";
                dbData.address_locality = address.address_locality ?? "";
                dbData.address_region = address.address_region ?? "";
                dbData.street_address = address.street_address ?? "";
                dbData.postal_code = address.postal_code ?? "";
            }
            await dbData.save();
        } catch (err) {
            throw new RecoverableError("Error while updating address.", this.constructor.name, err);
        }
    }

    public static async updateDistrict(dbData: PoliceStationsModel, cityDistrictsModel: CityDistrictsModel) {
        try {
            dbData.district = await cityDistrictsModel.getDistrict(
                dbData.geometry.coordinates[0],
                dbData.geometry.coordinates[1]
            );
            await dbData.save();
        } catch (err) {
            throw new RecoverableError("Error while updating district.", this.constructor.name, err);
        }
    }
}
