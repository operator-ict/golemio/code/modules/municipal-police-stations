import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { RefreshDataInDBTask, UpdateAddressAndDistrictTask } from "#ie/workers/tasks";

export class MunicipalPoliceStationsWorker extends AbstractWorker {
    protected readonly name = "MunicipalPoliceStations";

    constructor() {
        super();
        this.registerTask(new RefreshDataInDBTask(this.getQueuePrefix()));
        this.registerTask(new UpdateAddressAndDistrictTask(this.getQueuePrefix()));
    }
}
