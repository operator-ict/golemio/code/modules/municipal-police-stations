import { AbstractTask } from "@golemio/core/dist/integration-engine/workers";
import CityDistrictsModel from "@golemio/city-districts/dist/integration-engine/repositories/CityDistrictPostgresRepository";
import { AddressHelper } from "#ie/helpers/AddressHelper";
import { IUpdateDistrictInput, UpdateDistrictValidationSchema } from "#ie/workers/schemas/UpdateDistrictSchema";
import { PoliceStationsModel } from "#sch/models/PoliceStationsModel";
import { MunicipalPoliceStationsRepository } from "#ie/repositories";

export class UpdateAddressAndDistrictTask extends AbstractTask<IUpdateDistrictInput> {
    public readonly queueName = "updateAddressAndDistrict";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours
    public readonly schema = UpdateDistrictValidationSchema;

    public repository: MunicipalPoliceStationsRepository;
    private readonly cityDistrictsModel: CityDistrictsModel;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.repository = new MunicipalPoliceStationsRepository();
        this.cityDistrictsModel = new CityDistrictsModel();
    }

    protected async execute(msg: IUpdateDistrictInput) {
        const dbData: PoliceStationsModel = await this.repository.findOne({
            where: { id: msg.id },
        });
        await AddressHelper.updateDistrict(dbData, this.cityDistrictsModel);
        await AddressHelper.updateAddress(dbData);
    }
}
