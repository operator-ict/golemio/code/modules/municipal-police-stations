import { AbstractEmptyTask, QueueManager } from "@golemio/core/dist/integration-engine";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { MunicipalPoliceStationsTransformation } from "#ie/transformations/MunicipalPoliceStationsTransformation";
import { MunicipalPoliceStationsDataSourceFactory } from "#ie/datasources/MunicipalPoliceStationsDataSource";
import { MunicipalPoliceStationsRepository } from "#ie/repositories";
import { IMunicipalPoliceStation } from "#sch/MunicipalPoliceStations";
import { AddressHelper } from "#ie/helpers/AddressHelper";

export class RefreshDataInDBTask extends AbstractEmptyTask {
    public readonly queueName = "refreshDataInDB";
    public readonly queueTtl = 23 * 60 * 60 * 1000; // 23 hours

    private dataSource: DataSource;
    private transformation: MunicipalPoliceStationsTransformation;
    private repository: MunicipalPoliceStationsRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.dataSource = MunicipalPoliceStationsDataSourceFactory.getDataSource();
        this.transformation = new MunicipalPoliceStationsTransformation();
        this.repository = new MunicipalPoliceStationsRepository();
    }

    protected async execute(): Promise<void> {
        const data = await this.dataSource.getAll();
        const transformedData = await this.transformation.transform(data);

        const dataIdsWithNewLocation = await this.getStationIdsForAddressUpdate(transformedData);

        await this.repository.saveBulk(transformedData);

        const promises = dataIdsWithNewLocation.map((idObj) => {
            return QueueManager.sendMessageToExchange(this.queuePrefix, "updateAddressAndDistrict", idObj);
        });
        await Promise.all(promises);
    }

    private getStationIdsForAddressUpdate = async (
        transformedData: IMunicipalPoliceStation[]
    ): Promise<Array<{ id: string }>> => {
        const dataIdsWithNewLocation: Array<{ id: string }> = [];

        for (let index = 0; index < transformedData.length; index++) {
            const station = transformedData[index];
            const dbStation = await this.repository.findOne({ where: { id: station.id } });
            // get internal ids for already existing data
            if (dbStation) {
                station.id = dbStation.id;
                if (AddressHelper.needsUpdate(station, dbStation)) {
                    dataIdsWithNewLocation.push({ id: station.id });
                }
            } else {
                // new station
                dataIdsWithNewLocation.push({ id: station.id });
            }
        }

        return dataIdsWithNewLocation;
    };
}
