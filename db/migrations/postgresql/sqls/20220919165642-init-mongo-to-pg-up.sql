CREATE TABLE IF NOT EXISTS police_stations (
	"id" varchar NOT NULL,
	"geometry" geometry NULL,
    "cadastral_area" text NOT NULL,
    "note" text NOT NULL,
	"district" text NULL,

    -- address fields,
    "address_region" varchar NULL,
    "address_country" varchar NULL,
	"address_formatted" text NULL,
	"address_locality" varchar NULL,
	"postal_code" varchar NULL,
	"street_address" varchar NULL,

    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT police_stations_pkey PRIMARY KEY ("id")
);

CREATE INDEX idx_police_stations_geom ON police_stations USING gist ("geometry");
CREATE INDEX police_stations_district_idx ON police_stations USING btree ("district");