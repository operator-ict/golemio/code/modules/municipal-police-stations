import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { readFileSync } from "fs";
import { MunicipalPoliceStationsTransformation } from "#ie/transformations/MunicipalPoliceStationsTransformation";
import { IMunicipalPoliceStationsFeature } from "#sch/datasources/MunicipalPoliceStationsJsonSchema";

chai.use(chaiAsPromised);

describe("MunicipalPoliceStationsTransformation", () => {
    let transformation: MunicipalPoliceStationsTransformation;
    let testSourceData: IMunicipalPoliceStationsFeature[];

    const transformedDataFixture = {
        id: "2020",
        geometry: {
            type: "Point",
            coordinates: [14.4314393930001, 50.08265599],
        },
        cadastral_area: "Nové Město",
        note: "OŘ MP Praha 1",
    };

    beforeEach(async () => {
        transformation = new MunicipalPoliceStationsTransformation();
        testSourceData = JSON.parse(
            readFileSync(__dirname + "/../data/municipalpolicestations-datasource.json", "utf8")
        ).features;
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("MunicipalPoliceStations");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should correctly transform element", async () => {
        const data = transformation["transformElement"](testSourceData[0]);
        expect(data).to.deep.equal(transformedDataFixture);
    });

    it("should correctly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        for (const item of data) {
            expect(Object.keys(item).length).to.equal(4);
            expect(item).to.have.property("geometry");
            expect(item).to.have.property("id");
            expect(item).to.have.property("cadastral_area");
            expect(item).to.have.property("note");
        }
    });
});
