import { createSandbox, SinonSandbox } from "sinon";
import { expect } from "chai";
import { config, PostgresConnector } from "@golemio/core/dist/integration-engine";
import { MunicipalPoliceStationsWorker } from "#ie/workers/MunicipalPoliceStationsWorker";

describe("MunicipalPoliceStationsWorker", () => {
    let sandbox: SinonSandbox;
    let worker: MunicipalPoliceStationsWorker;

    beforeEach(() => {
        sandbox = createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );
        sandbox.stub(config, "datasources").value({
            MunicipalPoliceStations: "http://municipal-police-stations.cz/",
        });

        worker = new MunicipalPoliceStationsWorker();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("getQueuePrefix", () => {
        it("should have correct queue prefix set", () => {
            const result = worker["getQueuePrefix"]();
            expect(result).to.contain(".municipalpolicestations");
        });
    });

    describe("getQueueDefinition", () => {
        it("should return correct queue definition", () => {
            const result = worker.getQueueDefinition();
            expect(result.name).to.equal("MunicipalPoliceStations");
            expect(result.queues.length).to.equal(2);
        });
    });

    describe("registerTask", () => {
        it("should have two tasks registered", () => {
            expect(worker["queues"].length).to.equal(2);

            expect(worker["queues"][0].name).to.equal("refreshDataInDB");
            expect(worker["queues"][1].name).to.equal("updateAddressAndDistrict");

            expect(worker["queues"][0].options.messageTtl).to.equal(23 * 60 * 60 * 1000);
            expect(worker["queues"][1].options.messageTtl).to.equal(23 * 60 * 60 * 1000);
        });
    });
});
