import sinon, { SinonSpy, SinonSandbox } from "sinon";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { GeocodeApi } from "@golemio/core/dist/integration-engine/helpers";

import { UpdateAddressAndDistrictTask } from "#ie/workers/tasks";
import { IMunicipalPoliceStation } from "#sch/MunicipalPoliceStations";

describe("UpdateAddressAndDistrictTask", () => {
    let sandbox: SinonSandbox;
    let task: UpdateAddressAndDistrictTask;
    let data0: IMunicipalPoliceStation;
    let data1: Record<string, any>;

    beforeEach(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub().returns({ commit: sandbox.stub() }),
            })
        );

        data0 = {
            id: "nove-mesto-opletalova-19",
            geometry: { type: "Point", coordinates: [14.553641626000058, 50.14545956000006] },
            cadastral_area: "Nové Město",
            note: "OŘ MP Praha 1",
        };

        data1 = {
            id: "other id 2",
            geometry: { type: "Point", coordinates: [14.553641626000058, 50.14545956000006] },
            cadastral_area: "other area",
            note: "other note",
            save: sandbox.stub().resolves(true),
        };

        task = new UpdateAddressAndDistrictTask("test.test");

        sandbox.stub(task["repository"], "findOne").callsFake(() => Promise.resolve(data1));
        sandbox.stub(task["cityDistrictsModel"], "findOne");
        sandbox.stub(GeocodeApi, "getAddressByLatLng");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by updateAddressAndDistrict method", async () => {
        await task["execute"]({ id: data0.id });
        sandbox.assert.calledOnce(task["repository"].findOne as SinonSpy);
        sandbox.assert.calledWith(task["repository"].findOne as SinonSpy, { where: { id: data0.id } });
        sandbox.assert.calledOnce(task["cityDistrictsModel"].findOne as SinonSpy);
        sandbox.assert.calledOnce(GeocodeApi.getAddressByLatLng as SinonSpy);
        sandbox.assert.calledTwice(data1.save);
    });
});
