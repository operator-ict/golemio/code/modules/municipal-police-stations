import { createSandbox, SinonSandbox, SinonSpy } from "sinon";
import { config, PostgresConnector, QueueManager } from "@golemio/core/dist/integration-engine";
import { RefreshDataInDBTask } from "#ie/workers/tasks";
import { IMunicipalPoliceStation } from "#sch/MunicipalPoliceStations";
import { IMunicipalPoliceStationsFeature } from "#sch/datasources/MunicipalPoliceStationsJsonSchema";

describe("RefreshDataInDBTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshDataInDBTask;
    let testData: IMunicipalPoliceStationsFeature[];
    let testTransformedData: IMunicipalPoliceStation[];

    beforeEach(() => {
        sandbox = createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );
        sandbox.stub(config, "datasources").value({
            MunicipalPoliceStations: "http://municipal-police-stations.cz/",
        });

        task = new RefreshDataInDBTask("test.municipalpolicestations");

        testData = [
            {
                type: "Feature",
                id: 2107,
                geometry: { type: "Point", coordinates: [14.553641626000058, 50.14545956000006] },
                properties: {
                    OBJECTID: 2107,
                    NKU: "Zličín",
                    NVPK: null,
                    CPOP: null,
                    POZN: "HÚ - Depo Zličín",
                    GLOBALID: "f895493d-97f0-4350-add9-13c101f48aae",
                    OR_MP: "hlídkový útvar",
                },
            },
        ];

        testTransformedData = [
            {
                id: "1",
                geometry: { type: "Point", coordinates: [14.553641626000058, 50.14545956000006] },
                cadastral_area: "Zličín",
                note: "HÚ - Depo Zličín",
            },
        ];

        sandbox.stub(task["dataSource"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(task["transformation"], "transform").callsFake(() => Promise.resolve(testTransformedData));
        sandbox.stub(task["repository"], "findOne").callsFake(() => Promise.resolve(testTransformedData[0]));
        sandbox.stub(task["repository"], "saveBulk").callsFake(() => Promise.resolve());
        sandbox.stub(QueueManager, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call the correct methods by refreshDataInDB method", async () => {
        await task["execute"]();
        sandbox.assert.calledOnce(task["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["transformation"].transform as SinonSpy);
        sandbox.assert.calledWith(task["transformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(task["repository"].saveBulk as SinonSpy);
        sandbox.assert.calledWith(task["repository"].saveBulk as SinonSpy, testTransformedData);
        sandbox.assert.callCount(QueueManager["sendMessageToExchange"] as SinonSpy, 1);

        for (const data of testTransformedData) {
            sandbox.assert.calledWith(
                QueueManager["sendMessageToExchange"] as SinonSpy,
                "test.municipalpolicestations",
                "updateAddressAndDistrict",
                { id: "1" }
            );
        }

        sandbox.assert.callOrder(
            task["dataSource"].getAll as SinonSpy,
            task["transformation"].transform as SinonSpy,
            task["repository"].saveBulk as SinonSpy,
            QueueManager["sendMessageToExchange"] as SinonSpy
        );
    });
});
