import { AddressHelper } from "#ie/helpers/AddressHelper";
import { PoliceStationsModel } from "#sch/models/PoliceStationsModel";
import { GeocodeApi } from "@golemio/core/dist/integration-engine";
import { expect } from "chai";
import sinon, { SinonSandbox } from "sinon";

describe("AddressHelper", () => {
    let sandbox: SinonSandbox;
    let policeStationTest: PoliceStationsModel;
    const testData = {
        address_formatted: "Komárovská 416/22, 19300 Hlavní město Praha-Horní Počernice, Česko",
        street_address: "Komárovská 416/22",
        postal_code: "19300",
        address_locality: "Hlavní město Praha",
        address_region: "Horní Počernice",
        address_country: "Česko",
    };

    before(() => {
        sandbox = sinon.createSandbox({ useFakeTimers: true });
        sandbox.stub(GeocodeApi, "getAddressByLatLng" as any).callsFake(() => {
            return Promise.resolve(testData);
        });

        policeStationTest = {} as PoliceStationsModel;
        policeStationTest.id = "test-id";
        policeStationTest.geometry = { coordinates: [0, 0], type: "Point" };
        policeStationTest.save = sandbox.stub();
    });

    after(() => {
        sandbox.restore();
    });

    it("updateAddress should update Police Station instance", async () => {
        await AddressHelper.updateAddress(policeStationTest);
        expect(policeStationTest.address_formatted).to.be.eq(testData.address_formatted);
        expect(policeStationTest.street_address).to.be.eq(testData.street_address);
        expect(policeStationTest.postal_code).to.be.eq(testData.postal_code);
        expect(policeStationTest.address_locality).to.be.eq(testData.address_locality);
        expect(policeStationTest.address_region).to.be.eq(testData.address_region);
        expect(policeStationTest.address_country).to.be.eq(testData.address_country);
    });
});
