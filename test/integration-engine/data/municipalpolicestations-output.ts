export const outputDataFixture = {
    geometry: {
        coordinates: [14.615406090000022, 50.08092354900003],
        type: "Point",
    },
    properties: {
        id: "bechovice-ceskobrodska-8",
        cadastral_area: "Běchovice",
        note: "Okr",
        district: null,
        updated_at: "2022-09-20T01:51:21.648Z",
        address: {
            address_formatted: "Českobrodská 8, 190 11 Hlavní město Praha-Běchovice, Česko",
            street_address: "Českobrodská 8",
            postal_code: "190 11",
            address_locality: "Hlavní město Praha",
            address_region: "Běchovice",
            address_country: "Česko",
        },
    },
    type: "Feature",
};
