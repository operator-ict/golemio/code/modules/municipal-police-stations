import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { createSandbox, SinonSandbox } from "sinon";
import { MunicipalPoliceStationsRepository } from "#og";

chai.use(chaiAsPromised);

describe("MunicipalPoliceStationsRepository", () => {
    let sandbox: SinonSandbox;
    let municipalPoliceStationsRepository: MunicipalPoliceStationsRepository;

    before(() => {
        sandbox = createSandbox();
        municipalPoliceStationsRepository = new MunicipalPoliceStationsRepository();
    });

    after(() => {
        sandbox.restore();
    });

    it("should instantiate", () => {
        expect(municipalPoliceStationsRepository).not.to.be.undefined;
    });

    it("should return all items", async () => {
        const result = await municipalPoliceStationsRepository.GetAll();
        expect(result).to.be.an.instanceOf(Object);
        expect(result.features).to.be.an.instanceOf(Array);
        expect(result.features).to.have.length(4);
    });

    it("should return single item", async () => {
        const id = "bechovice-ceskobrodska-8";
        const result = await municipalPoliceStationsRepository.GetOne(id);
        expect(result).not.to.be.empty;
        expect(result!.properties).to.have.property("id", id);
        expect(result!.properties).to.have.property("address");
    });
});
